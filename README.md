
# Gifts Marketplace App 1.0
##### Develop By Muhamad Wibawa

## Langkah-langkah Install :

Install Node Js terlebih dahulu (Jika belum ada di laptop / PC Anda) dengan mengikuti langkah-langkah pada link berikut ini :

https://phoenixnap.com/kb/install-node-js-npm-on-windows

https://www.geeksforgeeks.org/installation-of-node-js-on-linux/

https://nodesource.com/blog/installing-nodejs-tutorial-mac-os-x/

Setelah Node Js terpasang, karena development aplikasi menggunakan yarn maka harus install tools tersebut sebagai berikut. Buka command prompt (Terminal) 
```
npm install --global yarn
```
Install juga Vue Js Secar global sebagai berikut :
```
yarn global add @vue/cli
```
Pastikan Vue yang digunakan adalah Vue version 2.4+ dengan cara mengetikan :
```
vue --version
```
Jika Vue bukan versi 2.4+ silahkan ikuti cara install pada link berikut :

https://cli.vuejs.org/guide/installation.html

Jika Vue sudah sesuai maka lanjutkan dengan melakukan clone repository aplikasi
 ```
 git clone https://[username-anda]@bitbucket.org/wibawamw/gifts.git
 ```
Setelah repository di download maka akan muncul folder gifts, buka folder tersebut melalui command prompt / terminal
```
 cd /path/to/folder/gifts
 yarn install
 ```
Untuk melakukan pengetesan dalam Environment Development maka lakukan
```
 yarn serve
 ```
Untuk melakukan pengetesan dalam Environment Production maka lakukan
```
 yarn build
 ```
Kemudian akan terbentuk semua file dalam folder dist (di dalam Folder gifts). Copy paste semua file tersebut ke suatu folder di Web Server (Misalkan Apache di htdocs).
Jalankan web server dan akses aplikasi, dengan cara-cara yang sama seperti berikut ini.  

https://www.edureka.co/blog/how-to-run-a-php-program-in-xampp/

## Tech Specification :
Tools yang dipergunakan adalah sebagai berikut :

 - VueJs, karena lebih mudah dipelajari (Dibandingkan ReactJS / Angular) dan lebih cepat dalam development. Jika dibandingkan dengan JQuery library sudah terpusat, tidak usah cari-cari yang lain
 - Bootstrap Vue, memilih bootstrap karena penggunaan nya simple, cocok dengan tugas yang diberikan. Dalam arti tidak terlalu banyak customisasi 

## Perubahan dalam Design :

 1. Filter yang sebelah Kiri menggunakan switch checkbox karena design nya lebih modern dibandingkan dengan checkbox biasa dan letak tombol di sebelah kiri (harus di konfigurasi jika ingin sebelah kanan, akan memakan waktu lama)
 2. FIlter untuk tampilan mobile (ukuran kecil) menggunakan sidebar yang bisa di slide dengan muncul dari kanan dengan menekan tombol di kanan atas
 3. Tombol Favorite (Hearth Icon) di ubah menjadi warna putih, jika wishlist berubah akan berubah jadi warna merah icon hati nya
 
 ## API yang tidak bisa di jalankan :
 
 https://recruitment.dev.rollingglory.com/api/v2/gifts/1/wishlist

Pada saat update wishlist sudah saya coba beberapa kali tetap field isWishList tidak berubah.

**Selamat Mencoba**

**Best Regard**

**Muhamad Wibawa**

import Vue from "vue";
import VueRouter from "vue-router";
import ProductList from "../views/ProductList.vue";
import ProductDetail from "../views/ProductDetail.vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

import productlistLayout from "../layouts/product-list-layout";
import productdetailLayout from "../layouts/product-detail-layout";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-reboot.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(VueRouter);

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

const routes = [
  {
    path: "/",
    name: "productlist",
    components: {
      layout: productlistLayout,
      content: ProductList,
    },
  },
  {
    path: "/product-detail/:id",
    name: "productdetail",
    components: {
      layout: productdetailLayout,
      content: ProductDetail,
    },
    props: true,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;

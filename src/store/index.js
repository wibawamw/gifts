import Vue from "vue";
import Vuex from "vuex";
import numeral from "numeral";

Vue.use(Vuex);

numeral.register("locale", "id", {
  delimiters: {
    thousands: ".",
    decimal: ",",
  },
  abbreviations: {
    thousand: "ribu",
    million: "juta",
    billion: "milyar",
    trillion: "trilyun",
  },
  ordinal: function (number) {
    return number === 1 ? "er" : "ème";
  },
  currency: {
    symbol: "Rp.",
  },
});

numeral.locale("id");

export default new Vuex.Store({
  state: {
    gifts: [],
    gift: null,
    tempGifts: [],
    filter: {
      rating: false,
      available: false,
    },
    sortBy: "Terbaru",
  },
  getters: {
    gifts: (state) => {
      return state.gifts;
    },
    gift: (state) => {
      return state.gift;
    },
    filter: (state) => {
      return state.filter;
    },
    sortBy: (state) => {
      return state.sortBy;
    },
  },
  mutations: {
    setGifts: (state, gifts) => {
      gifts.forEach(function (item) {
        let data = item.attributes;

        if (data.stock > 5) {
          data.stockStatus = "In Stock";
          data.stockCSS = "success";
        } else if (data.stock < 5) {
          data.stockStatus = "Stock < 5";
          data.stockCSS = "danger";
        } else {
          data.stockStatus = "Sold Out";
          data.stockCSS = "muted";
        }

        data.pointStr = numeral(data.points).format("0,0");

        data.wishListCSS = data.isWishlist == 1 ? "heart-fill" : "heart";
        data.ribbonImage = "";
      });

      // Sorting First

      if (state.sortBy == "Terbaru") {
        gifts.sort((a, b) =>
          a.attributes.isNew < b.attributes.isNew
            ? 1
            : b.attributes.isNew < a.attributes.isNew
            ? -1
            : 0
        );
      } else if (state.sortBy == "Ulasan") {
        gifts.sort((a, b) =>
          a.attributes.numOfReviews < b.attributes.numOfReviews
            ? 1
            : b.attributes.numOfReviews < a.attributes.numOfReviews
            ? -1
            : 0
        );
      }

      state.gifts = gifts;
    },
    clearGifts: (state) => {
      state.customer = null;
    },
    addGift: (state, gift) => {
      let data = gift.attributes;

      if (data.stock > 5) {
        data.stockStatus = "In Stock";
        data.stockCSS = "success";
      } else if (data.stock < 5) {
        data.stockStatus = "Stock < 5";
        data.stockCSS = "danger";
      } else {
        data.stockStatus = "Sold Out";
        data.stockCSS = "muted";
      }

      data.pointStr = numeral(data.points).format("0,0");

      data.wishListCSS = data.isWishlist == 1 ? "heart-fill" : "heart";
      state.gifts.push(gift);
    },
    setGift: (state, gift) => {
      let data = gift.attributes;

      if (data.stock > 5) {
        data.stockStatus = "In Stock";
        data.stockCSS = "text-green";
      } else if (data.stock < 5) {
        data.stockStatus = "Stock < 5";
        data.stockCSS = "text-danger";
      } else {
        data.stockStatus = "Sold Out";
        data.stockCSS = "text-muted";
      }

      data.pointStr = numeral(data.points).format("0,0");

      data.wishListCSS = data.isWishlist == 1 ? "heart-fill" : "heart";
      state.gift = gift;
    },
    filterGifts: (state) => {
      if (state.filter.rating || state.filter.available) {
        if (state.tempGifts.length > 0) {
          state.gifts = JSON.parse(JSON.stringify(state.tempGifts));
        }

        state.tempGifts = JSON.parse(JSON.stringify(state.gifts));
        let giftTmp = [];
        state.gifts.forEach(function (item) {
          let data = item.attributes;

          if (state.filter.rating && state.filter.available) {
            if (data.rating > 4 && data.stock > 0) {
              giftTmp.push(item);
            }
          } else if (state.filter.rating && !state.filter.available) {
            if (data.rating > 4) {
              giftTmp.push(item);
            }
          } else if (!state.filter.rating && state.filter.available) {
            if (data.stock > 0) {
              giftTmp.push(item);
            }
          }
        });
        state.gifts = JSON.parse(JSON.stringify(giftTmp));
      } else {
        state.gifts = JSON.parse(JSON.stringify(state.tempGifts));
      }
    },

    setFilter: (state, filter) => {
      state.filter = filter;
    },

    clearFilter: (state) => {
      state.filter = {
        rating: false,
        available: false,
      };
    },
    setSortBy: (state, sortBy) => {
      state.sortBy = sortBy;
    },

    clearSortBy: (state) => {
      state.sortBy = "Terbaru";
    },
    sortGifts: (state) => {
      if (state.sortBy == "Terbaru") {
        state.gifts.sort((a, b) =>
          a.attributes.isNew < b.attributes.isNew
            ? 1
            : b.attributes.isNew < a.attributes.isNew
            ? -1
            : 0
        );
      } else if (state.sortBy == "Ulasan") {
        state.gifts.sort((a, b) =>
          a.attributes.numOfReviews < b.attributes.numOfReviews
            ? 1
            : b.attributes.numOfReviews < a.attributes.numOfReviews
            ? -1
            : 0
        );
      }
    },
  },
  actions: {
    setGifts({ commit }, data) {
      commit("setGifts", data);
    },
    clearGifts({ commit }) {
      commit("clearGifts");
    },
    addGift({ commit }, data) {
      commit("addGift", data);
    },
    setGift({ commit }, data) {
      commit("setGift", data);
    },
    filterGifts({ commit }) {
      commit("filterGifts");
    },
    setFilter({ commit }, data) {
      commit("setFilter", data);
    },
    clearFilter({ commit }) {
      commit("clearFilter");
    },
    setSortBy({ commit }, data) {
      commit("setSortBy", data);
    },
    clearSortBy({ commit }) {
      commit("clearSortBy");
    },
    sortGifts({ commit }) {
      commit("sortGifts");
    },
  },
  modules: {},
});

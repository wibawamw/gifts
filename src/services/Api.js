import axios from "axios";
import router from "../router";

export default () => {
  var api = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
    withCredentials: false,
  });

  api.interceptors.response.use(
    function (response) {
      return response;
    },
    function (error) {
      if (error.response.status == 401) {
        window.localStorage.removeItem("sikaduser");
        router.push("/login");
      }
      return Promise.reject(error);
    }
  );

  return api;
};

import Api from "@/services/Api";

export default {
  async getData(params) {
    var results = null;

    params.withResult = true;

    await Api()
      .get("gifts", {
        params: params,
      })
      .then((data) => {
        results = data.data;
        // console.log(results);
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });

    return results;
  },
  async getDataById(id, params) {
    var results = null;

    params.withResult = true;

    await Api()
      .get("gifts/" + id, {
        params: params,
      })
      .then((data) => {
        results = data.data;
        // console.log(results);
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });

    return results;
  },

  addToWishList(id, requestBody, callback, errCallback) {
    return Api()
      .post("gifts/" + id + "/wishlist", requestBody)
      .then((data) => {
        callback(data);
      })
      .catch((error) => {
        errCallback(error);
      });
  },
};
